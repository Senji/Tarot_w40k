package tarotTest;

import junit.framework.TestCase;
import org.junit.Assert;
import tarot.Carte;

/**
 * Created by vrichard on 22/04/2015.
 */
public class CarteTest extends TestCase {

    private Carte carte = new Carte("Atout", "4", "L'Empereur Dieu", "Arcane");

    public void testGetNomClassique() throws Exception {
        Assert.assertEquals("4", carte.getNomClassique());
    }

    public void testGetNomTarot() throws Exception {
        Assert.assertEquals("L'Empereur Dieu", carte.getNomTarot());
    }

    public void testGetCouleur() throws Exception {
        Assert.assertEquals("Atout", carte.getCouleur());
    }

    public void testGetCouleurTarot() throws Exception {
        Assert.assertEquals("Arcane", carte.getCouleurTarot());
    }

    public void testGetAugure() throws Exception {
        Assert.assertEquals(3, carte.getAugure());
    }

    public void testIsBonneAugure() throws Exception {
        Assert.assertEquals(true, carte.isBonneAugure());
    }

}