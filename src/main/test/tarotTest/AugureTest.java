package tarotTest;

import junit.framework.TestCase;
import org.junit.Assert;

import java.util.ArrayList;

import tarot.Carte;
import tarot.Augure;

/**
 * Created by vrichard on 22/04/2015.
 */
public class AugureTest extends TestCase {

    public void testCalcul() throws Exception {
        Carte asc = new Carte("Atout", "11", "Juge", "Arcane");
        ArrayList<Carte> aug = new ArrayList<>();
        aug.add(new Carte("Atout", "19", "Sol", "Arcane"));
        aug.add(new Carte("Coeur", "As", "Terra", "Adeptio"));
        aug.add(new Carte("Pique", "5", "Le Sauvage", "Discordia"));
        Augure augure = new Augure(asc, aug);

        Assert.assertEquals("Bonne augure", augure.getAugureString());
        Assert.assertEquals(1, augure.nbAtoutBon);
        Assert.assertEquals(1, augure.nbCouleurBonne);
        Assert.assertEquals(1, augure.nbCouleurMauvaise);
        Assert.assertEquals(0, augure.nbAtoutMauvais);
        int[] calculCarte = augure.calculCarte();
        Assert.assertEquals(augure.augure, calculCarte[0]);
        Assert.assertEquals(augure.nbAtoutBon, calculCarte[1]);
        Assert.assertEquals(augure.nbCouleurBonne, calculCarte[2]);
        Assert.assertEquals(augure.nbAtoutMauvais, calculCarte[3]);
        Assert.assertEquals(augure.nbCouleurMauvaise, calculCarte[4]);


        asc = new Carte("Coeur", "Cavalier", "Le Commissaire", "Adeptio");
        aug.clear();
        aug.add(new Carte("Atout", "4", "L'Empereur Dieu", "Arcane"));
        aug.add(new Carte("Pique", "Dame", "La Sorcière", "Discordia"));
        aug.add(new Carte("Pique", "5", "Le Sauvage", "Discordia"));
        augure = new Augure(asc, aug);

        Assert.assertEquals("Bonne augure", augure.getAugureString());
        Assert.assertEquals(1, augure.nbAtoutBon);
        Assert.assertEquals(0, augure.nbCouleurBonne);
        Assert.assertEquals(2, augure.nbCouleurMauvaise);
        Assert.assertEquals(0, augure.nbAtoutMauvais);
        calculCarte = augure.calculCarte();
        Assert.assertEquals(augure.augure, calculCarte[0]);
        Assert.assertEquals(augure.nbAtoutBon, calculCarte[1]);
        Assert.assertEquals(augure.nbCouleurBonne, calculCarte[2]);
        Assert.assertEquals(augure.nbAtoutMauvais, calculCarte[3]);
        Assert.assertEquals(augure.nbCouleurMauvaise, calculCarte[4]);


        asc = new Carte("Trèfle", "As", "La Nova", "Executera");
        aug.clear();
        aug.add(new Carte("Atout", "16", "Le Démon", "Arcane"));
        aug.add(new Carte("Coeur", "As", "Terra", "Adeptio"));
        aug.add(new Carte("Carreau", "5", "La Dîme", "Mandatio"));
        augure = new Augure(asc, aug);

        Assert.assertEquals("Mauvaise augure", augure.getAugureString());
        Assert.assertEquals(0, augure.nbAtoutBon);
        Assert.assertEquals(2, augure.nbCouleurBonne);
        Assert.assertEquals(0, augure.nbCouleurMauvaise);
        Assert.assertEquals(1, augure.nbAtoutMauvais);
        calculCarte = augure.calculCarte();
        Assert.assertEquals(augure.augure, calculCarte[0]);
        Assert.assertEquals(augure.nbAtoutBon, calculCarte[1]);
        Assert.assertEquals(augure.nbCouleurBonne, calculCarte[2]);
        Assert.assertEquals(augure.nbAtoutMauvais, calculCarte[3]);
        Assert.assertEquals(augure.nbCouleurMauvaise, calculCarte[4]);


        asc = new Carte("Trèfle", "As", "La Nova", "Executera");
        aug.clear();
        aug.add(new Carte("Atout", "16", "Le Démon", "Arcane"));
        aug.add(new Carte("Atout", "4", "L'Empereur Dieu", "Arcane"));
        aug.add(new Carte("Carreau", "5", "La Dîme", "Mandatio"));
        augure = new Augure(asc, aug);

        Assert.assertEquals("Bonne augure", augure.getAugureString());
        Assert.assertEquals(1, augure.nbAtoutBon);
        Assert.assertEquals(1, augure.nbCouleurBonne);
        Assert.assertEquals(0, augure.nbCouleurMauvaise);
        Assert.assertEquals(1, augure.nbAtoutMauvais);
        calculCarte = augure.calculCarte();
        Assert.assertEquals(augure.augure, calculCarte[0]);
        Assert.assertEquals(augure.nbAtoutBon, calculCarte[1]);
        Assert.assertEquals(augure.nbCouleurBonne, calculCarte[2]);
        Assert.assertEquals(augure.nbAtoutMauvais, calculCarte[3]);
        Assert.assertEquals(augure.nbCouleurMauvaise, calculCarte[4]);


        asc = new Carte("Coeur", "Dame", "La Sororité", "Adeptio");
        aug.clear();
        aug.add(new Carte("Atout", "16", "Flèche du Destin", "Arcane"));
        aug.add(new Carte("Atout", "4", "Le Vaisseau", "Arcane"));
        aug.add(new Carte("Atout", "5", "Exterminatus", "Arcane"));
        augure = new Augure(asc, aug);

        calculCarte = augure.calculCarte();
        if (augure.augure != calculCarte[0] ||
                augure.nbAtoutBon != calculCarte[1] ||
                augure.nbCouleurBonne != calculCarte[2] ||
                augure.nbAtoutMauvais != calculCarte[3] ||
                augure.nbCouleurMauvaise != calculCarte[4]) {
            System.out.println("Des différences existent");
        }
        if (augure.augure > 0) {
            Assert.assertEquals("Bonne augure", augure.getAugureString());
        } else {
            Assert.assertEquals("Mauvaise augure", augure.getAugureString());
        }
    }
}