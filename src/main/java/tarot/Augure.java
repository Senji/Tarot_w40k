package tarot;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;

import tarot.Carte.Etat;

/**
 * Created by vrichard on 03/04/2015.
 */
public class Augure {
    public Carte ascendante;
    public ArrayList<Carte> tirage;
    public int augure, nbCouleurBonne, nbAtoutBon, nbCouleurMauvaise, nbAtoutMauvais;
    public Random rand;

    public Augure(Carte ascendante, ArrayList<Carte> tirage) {
        this.ascendante = ascendante;
        this.tirage = tirage;
        this.rand = new Random();
        this.augure = 0;
        this.nbCouleurBonne = 0;
        this.nbAtoutBon = 0;
        this.nbCouleurMauvaise = 0;
        this.nbAtoutMauvais = 0;
        calculCarte();
    }

    public String getAugureString() {
        if (this.augure > 0) {
            return "Bonne augure";
        }
        return "Mauvaise augure";
    }

    @Override
    public String toString() {
        return "Augure{" +
                "ascendante=" + ascendante.toStringTarot() +
                ", augure=" + getAugureString() +
                ", nbCouleurBonne=" + nbCouleurBonne +
                ", nbAtoutBon=" + nbAtoutBon +
                ", nbCouleurMauvaise=" + nbCouleurMauvaise +
                ", nbAtoutMauvais=" + nbAtoutMauvais +
                '}';
    }

    public int[] calculCarte() {
        int resultAugure = 0, bonAtout = 0, bonneCouleur = 0, mauvaisAtout = 0, mauvaiseCouleur = 0;
        for (Carte carte : tirage) {
            resultAugure += carte.getAugure();
            switch (carte.etat) {
                case BON_ATOUT:
                    bonAtout++;
                    break;
                case MAUVAIS_ATOUT:
                    mauvaisAtout++;
                    break;
                case BONNE_COULEUR:
                    bonneCouleur++;
                    break;
                case MAUVAISE_COULEUR:
                    mauvaiseCouleur++;
                    break;
            }
        }
        this.augure = resultAugure;
        this.nbAtoutBon = bonAtout;
        this.nbAtoutMauvais = mauvaisAtout;
        this.nbCouleurBonne = bonneCouleur;
        this.nbCouleurMauvaise = mauvaiseCouleur;
        int[] result = {resultAugure, bonAtout, bonneCouleur, mauvaisAtout, mauvaiseCouleur};
        return result;
    }

}
