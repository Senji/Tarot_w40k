package tarot;

import java.util.Random;

public class Carte {

    public enum Etat{
        MAUVAIS_ATOUT,
        BON_ATOUT,
        MAUVAISE_COULEUR,
        BONNE_COULEUR,
        NON_DEFINI
    }

    private String nomClassique;
    private String nomTarot;
    private String couleur;
    private String couleurTarot;
    private int augure;
    private Random rand;
    public Etat etat;

    public Carte(String couleur, String nomClass, String nomTar, String colTar) {
        rand = new Random();
        if (couleur.equals("Atout")) {
            this.nomClassique = nomClass;
        } else {
            this.nomClassique = nomClass + " de " + couleur;
        }
        this.couleur = couleur;
        this.couleurTarot = colTar;
        this.nomTarot = nomTar;
        this.etat=Etat.NON_DEFINI;
        setAugure();
    }

    public String getNomClassique() {
        return nomClassique;
    }

    public String getNomTarot() {
        return nomTarot;
    }

    public String getCouleur() {
        return couleur;
    }

    public String getCouleurTarot() {
        return couleurTarot;
    }

    public int getAugure() {
        return augure;
    }

    public boolean isBonneAugure() {
        if (this.augure > 0) {
            return true;
        }
        // Le Pélerin ne sera jamais testé
        return false;
    }

    private void setAugure() {
        switch (this.getCouleurTarot()){
            case "Discordia":
                this.augure = -1;
                this.etat=Etat.MAUVAISE_COULEUR;
                break;
            case "Arcane":
                switch (this.getNomTarot()) {
                    case "Le Traître":
                    case "Luna":
                        this.augure = -1;
                        this.etat=Etat.MAUVAIS_ATOUT;
                        break;
                    case "L'Empereur Dieu":
                        this.augure = 3;
                        this.etat=Etat.BON_ATOUT;
                        break;
                    case "Le Pélerin":
                        this.augure = 0;
                        break;
                    case "Le Démon":
                        this.augure = -3;
                        this.etat=Etat.MAUVAIS_ATOUT;
                        break;
                    case "Flèche du Destin":
                    case "Le Vaisseau":
                    case "Exterminatus":
                        int rnd = rand.nextInt(10);
                        if (rnd < 5) {
                            this.augure = 1;
                            this.etat=Etat.BON_ATOUT;
                        } else {
                            this.augure = -1;
                            this.etat=Etat.MAUVAIS_ATOUT;
                        }
                        break;
                    default:
                        this.augure = 1;
                        this.etat=Etat.BON_ATOUT;
                }
                break;
            default:
                this.augure = 1;
                this.etat=Etat.BONNE_COULEUR;
        }
    }

    @Override
    public String toString() {
        return "Carte [nomClassique=" + nomClassique
                + ", couleur=" + couleur
                + ", nomTarot=" + nomTarot
                + ", couleurTarot=" + couleurTarot
                + "]";
    }

    public String toStringTarot() {
        return "Carte [nomTarot=" + nomTarot
                + ", couleurTarot=" + couleurTarot
                + "]";
    }

}
