package tarot;

import java.util.ArrayList;
import java.util.Random;
import java.util.TreeMap;

public class Tirage {

    static Random rand;
    static ArrayList<Carte> paquetCarte;

    public static void main(String[] args) {

        paquetCarte = new ArrayList<>();
        ArrayList<Carte> deck = new ArrayList<>();
        rand = new Random();

        System.out
                .println("Lancement des initialisations et remplissage du paquet->");
        initArcane();
        initCoeur();
        initPique();
        initTrefle();
        initCarreau();
        while (paquetCarte.size() != deck.size()) {
            int indiceCarte = rand.nextInt(paquetCarte.size());
            deck.add(paquetCarte.get(indiceCarte));
        }
        System.out.println("<- Initialisations et remplissage terminées.");
        System.out.println("\tTaille du paquet : " + deck.size());
        System.out.println();

        System.out.println("Tirage ->");
        tirage(deck, new ArrayList<Carte>());

    }

    public static void tirage(ArrayList<Carte> deckCarte, ArrayList<Carte> tirage) {
        Carte ascendante;
        ArrayList<Carte> tirageProphete = new ArrayList<>();

        // Tirage de l'Ascendante
        ascendante = getFirstCarte(deckCarte);
        deckCarte.remove(0);
        System.out.println("\tAscendante : " + ascendante);
        System.out.println();

        while (tirage.size() < 3) {
            tirage = tirageAugure(deckCarte, tirage);
            deckCarte.remove(0);
            if (tirage.size() != 3 && tirage.get(tirage.size() - 1).getNomTarot().equals("Le Prophète")) {
                tirageProphete = tirage;
                ArrayList<Carte> deckFinDebut = new ArrayList<>();
                deckFinDebut.add(getLastCarte(deckCarte));
                while (deckFinDebut.size() != deckCarte.size()) {
                    deckFinDebut.add(getFirstCarte(deckCarte));
                }
                while (tirageProphete.size() != 3) {
                    tirageProphete = tirageAugure(deckFinDebut, tirageProphete);
                    deckFinDebut.remove(0);
                }
            } else if (tirage.get(tirage.size() - 1).getNomTarot().equals("Le Prophète")) {
                System.out.println("\tC'était la dernière augure.");
            }
        }

        System.out.println("<- Résultat de l'Augure");
        Augure augureFinal = new Augure(ascendante, tirage);
        System.out.println(augureFinal.toString());

        if (tirageProphete.size() != 0) {
            System.out.println("<- Résultat de l'Augure du Prophète");
            augureFinal = new Augure(ascendante, tirageProphete);
            System.out.println(augureFinal.toString());
        }
    }

    public static ArrayList<Carte> tirageAugure(ArrayList<Carte> deck, ArrayList<Carte> preTirage) {
        Carte debutDeck = getFirstCarte(deck);
        switch (debutDeck.getNomTarot()){
            case "Le Pélerin":
                System.out.println("\tTirage du Pélerin");
                if (preTirage.size() > 0) {
                    System.out.println("\tRetrait de la carte : " + preTirage.get(preTirage.size() - 1));
                    preTirage.remove(preTirage.size() - 1);
                } else {
                    System.out.println("\tPas de changement car c'était le première carte");
                }
                break;
            case "Le Prophète":
                System.out.println("\tTirage du Prohpète");
                preTirage.add(debutDeck);
                System.out.println("\tAugure n°" + preTirage.size() + " : " + preTirage.get(preTirage.size() - 1));
                break;
            default:
                preTirage.add(debutDeck);
                System.out.println("\tAugure n°" + preTirage.size() + " : " + preTirage.get(preTirage.size() - 1));
        }
        return preTirage;
    }

    public static Carte getFirstCarte(ArrayList<Carte> deckCarte) {
        return deckCarte.get(0);
    }

    public static Carte getLastCarte(ArrayList<Carte> deckCarte) {
        return (deckCarte.get(deckCarte.size() - 1));
    }

    public static void initArcane() {
        System.out.println("\tInitialisation des arcanes.");
        TreeMap<String, String> arcanes = new TreeMap<>();
        arcanes.put("Excuse", "Pélerin");
        arcanes.put("1", "Psyker");
        arcanes.put("2", "Prophète");
        arcanes.put("3", "Sainte");
        arcanes.put("4", "L'Empereur Dieu");
        arcanes.put("5", "L'Ecclésiaste");
        arcanes.put("6", "Le Primarque");
        arcanes.put("7", "Le Vaisseau");
        arcanes.put("8", "Le Titan");
        arcanes.put("9", "L'Arch Magos");
        arcanes.put("10", "La Flèche du Destin");
        arcanes.put("11", "Le Juge");
        arcanes.put("12", "Le Martyr");
        arcanes.put("13", "Exterminatus");
        arcanes.put("14", "Le Rogue Trader");
        arcanes.put("15", "L'Ennemi");
        arcanes.put("16", "Le Démon");
        arcanes.put("17", "La Constellation");
        arcanes.put("18", "Luna");
        arcanes.put("19", "Sol");
        arcanes.put("20", "Le Jugement");
        arcanes.put("21", "La Galaxie");

        String couleur = "Atout";
        String couleurTar = "Arcane";
        for (String key : arcanes.keySet()) {
            paquetCarte.add(new Carte(couleur, key, arcanes.get(key), couleurTar));
        }
        System.out.println("\tArcanes initialisées.");
    }

    public static void initCoeur() {
        System.out.println("\tInitialisation des Coeurs/Adeptia.");
        TreeMap<String, String> coeurAdeptio = new TreeMap<>();
        coeurAdeptio.put("As", "Terra");
        coeurAdeptio.put("2", "Le Soigneur");
        coeurAdeptio.put("3", "L'assassin");
        coeurAdeptio.put("4", "L'Archiviste");
        coeurAdeptio.put("5", "Le Perdu");
        coeurAdeptio.put("6", "Le Serviteur");
        coeurAdeptio.put("7", "Le Citoyen");
        coeurAdeptio.put("8", "La Brute");
        coeurAdeptio.put("9", "Le Guerrier");
        coeurAdeptio.put("10", "L'Agent");
        coeurAdeptio.put("Valet", "Le Chapelin");
        coeurAdeptio.put("Cavalier", "Le Commissaire");
        coeurAdeptio.put("Dame", "La Sororité");
        coeurAdeptio.put("Roi", "L'Astartes");

        String couleur = "Coeur";
        String couleurTar = "Adeptio";
        for (String key : coeurAdeptio.keySet()) {
            paquetCarte.add(new Carte(couleur, key, coeurAdeptio.get(key), couleurTar));
        }
        System.out.println("\tCoeurs/Adeptia initialisés.");
    }

    public static void initPique() {
        System.out.println("\tInitialisation des Piques/Discordia.");
        TreeMap<String, String> piqueDiscordia = new TreeMap<>();
        piqueDiscordia.put("As", "L'Arlequin");
        piqueDiscordia.put("2", "L'Oeil de la Terreur");
        piqueDiscordia.put("3", "Le Rénégat");
        piqueDiscordia.put("4", "Le Mutant");
        piqueDiscordia.put("5", "Le Sauvage");
        piqueDiscordia.put("6", "L'Hédoniste");
        piqueDiscordia.put("7", "Le Porteur de Désespoir");
        piqueDiscordia.put("8", "Le Seigneur des Batailles");
        piqueDiscordia.put("9", "Le Changeur de Voie");
        piqueDiscordia.put("10", "L'Egaré");
        piqueDiscordia.put("Valet", "Le Traitre");
        piqueDiscordia.put("Cavalier", "L'Enigme");
        piqueDiscordia.put("Dame", "La Sorcière");
        piqueDiscordia.put("Roi", "Le Kraken");

        String couleur = "Pique";
        String couleurTar = "Discordia";
        for (String key : piqueDiscordia.keySet()) {
            paquetCarte.add(new Carte(couleur, key, piqueDiscordia.get(key), couleurTar));
        }
        System.out.println("\tPiques/Discordia initialisés.");
    }

    public static void initTrefle() {
        System.out.println("\tInitialisation des Trèfles/Executera.");
        TreeMap<String, String> trefleExecutera = new TreeMap<>();
        trefleExecutera.put("As", "La Nova");
        trefleExecutera.put("2", "Le Navigateur");
        trefleExecutera.put("3", "L'Arbites");
        trefleExecutera.put("4", "La Paix de l'Empeureur");
        trefleExecutera.put("5", "La Navis Nobilite");
        trefleExecutera.put("6", "La Victoire");
        trefleExecutera.put("7", "Le Contrat");
        trefleExecutera.put("8", "Le Duel");
        trefleExecutera.put("9", "Le Fabrictor");
        trefleExecutera.put("10", "Le Conflit");
        trefleExecutera.put("Valet", "L'Emissaire");
        trefleExecutera.put("Cavalier", "Le Magus");
        trefleExecutera.put("Dame", "La Surveillante");
        trefleExecutera.put("Roi", "L'Astronomicon");

        String couleur = "Trèfle";
        String couleurTar = "Executera";
        for (String key : trefleExecutera.keySet()) {
            paquetCarte.add(new Carte(couleur, key, trefleExecutera.get(key), couleurTar));
        }
        System.out.println("\tTrèfles/Executera initialisés.");
    }

    public static void initCarreau() {
        System.out.println("\tInitialisation des Carreaux/Mandatio.");
        TreeMap<String, String> carreauMandatio = new TreeMap<>();
        carreauMandatio.put("As", "La Ruche");
        carreauMandatio.put("2", "Le Livre de Compte");
        carreauMandatio.put("3", "Le Gouverneur");
        carreauMandatio.put("4", "L'Enforcer");
        carreauMandatio.put("5", "La Dîme");
        carreauMandatio.put("6", "L'Adepte");
        carreauMandatio.put("7", "La Schola Progenia");
        carreauMandatio.put("8", "L'Administratum");
        carreauMandatio.put("9", "Le Cogitator");
        carreauMandatio.put("10", "Le Scribe");
        carreauMandatio.put("Valet", "Le Haut Prêtre");
        carreauMandatio.put("Cavalier", "L'Inquisiteur");
        carreauMandatio.put("Dame", "La Chanoinesse");
        carreauMandatio.put("Roi", "L'Omnissiah");

        String couleur = "Carreau";
        String couleurTar = "Mandatio";
        for (String key : carreauMandatio.keySet()) {
            paquetCarte.add(new Carte(couleur, key, carreauMandatio.get(key), couleurTar));
        }
        System.out.println("\tCarreaux/Mandatio initialisés.");
    }

}
